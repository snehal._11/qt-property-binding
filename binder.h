#ifndef BINDER_H
#define BINDER_H

#include <QObject>
//#include <QLineEdit>
#include <control.h>
#include <QDebug>
#include <QDialog>
#include <QMessageBox>

#include "property.h"
#include "qpushbutton.h"

class CBinder : public QObject {
  Q_OBJECT
 public:
  CBinder(IProperty* prop,
          IControl* control,
          QPushButton* pushButton = nullptr)
    :
    m_prop(prop),
    m_control(control),
    m_pushButton(pushButton) {
    connect(m_control, SIGNAL(dataChanged(const QString&)), this, SLOT(onDataChanged(const QString&)));
    connect(m_prop, SIGNAL(propertyChanged()), this, SLOT(onPropertyChanged()));
    if (m_pushButton) {
      connect(m_pushButton, SIGNAL(clicked()), this, SLOT(onClicked()));
    }
    connect(m_prop, SIGNAL(statusChanged()), this, SLOT(onStatusChanged()));
  }
  IProperty::Status getPropertyStatus()const {
    return m_prop->getStatus();
  }

  void init() {
    *m_prop = m_control->getData();
  }

 signals:
  void statusChanged();

 private:
  IProperty*    m_prop;
  IControl*     m_control;
  QPushButton* m_pushButton;

 private slots:
  void onDataChanged(const QString& txt) {
    *m_prop = txt;
  }

  void onPropertyChanged() {
    QString currentVal = m_prop->getValue();
    m_control->setData(currentVal);
  }

  void onStatusChanged() {
    if (m_pushButton != nullptr) {
      if (m_prop->getStatus() == IProperty::Status::Valid) {
        m_pushButton->setVisible(false);
      } else     if (m_prop->getStatus() == IProperty::Status::Invalid) {
        m_pushButton->setVisible(true);
      }
    }
    emit statusChanged();
  }

  void onClicked() {
    QMessageBox msgBox;
    QString str = m_prop->isPropertyValid();
    if (str.length() > 0) {
      qDebug() << str << "length:";
      msgBox.setWindowTitle("Error");
      msgBox.setText(str);
      msgBox.exec();
    }

  }


};

#endif // BINDER_H
