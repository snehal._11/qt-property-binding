#ifndef CLASSANDVARIABLETYPEPROPERTY_H
#define CLASSANDVARIABLETYPEPROPERTY_H

#include "classtypeproperty.h"
#include <QDebug>


template <class ClassType, class VariableType>
class ClassAndVariableTypeProperty : public ClassTypeProperty<ClassType> {
  typedef void (ClassType::*TypeSettorFunPtrTD)(const VariableType& val);
  typedef VariableType(ClassType::*TypeGetterFunPtrTD)() const;
  typedef QString(ClassType::*ValidatorFunPtrTD)(const VariableType& val) const;
 public:
  ClassAndVariableTypeProperty() {
    m_settor = nullptr;
    m_gettor = nullptr;
  }

  ClassAndVariableTypeProperty(const QString& name,
                               TypeSettorFunPtrTD settor,
                               TypeGetterFunPtrTD gettor,
                               ClassType* object,
                               ValidatorFunPtrTD validator = nullptr)
    :
    ClassTypeProperty<ClassType>(name, object) {
    m_settor = settor;
    m_gettor = gettor;
    m_validator = validator;
  }

  IProperty* operator = (const QString& val) {
    VariableType new_value;
    ConvertFromToString::convert(val, new_value);

    IProperty::Status old_status, new_status  = getStatus();


    VariableType old_value = (*(this->m_pObject).*m_gettor)();
    if (old_value != new_value) {
      QString error = "";
      if (m_validator != nullptr) {
        error = (*(this->m_pObject).*m_validator)(new_value);
      }
      if (error.length() == 0) {
        new_status = IProperty::Status::Valid;
      } else {
        new_status = IProperty::Status::Invalid;
      }
      setStatus(new_status);

      (*(this->m_pObject).*m_settor)(new_value);
      emit propertyChanged();
    }
    return this;
  }

  QString isPropertyValid() const {
    QString error = "";
    if (m_validator != nullptr) {
      VariableType val = (*(this->m_pObject).*m_gettor)();
      error = (*(this->m_pObject).*m_validator)(val);
    }
    return error;
  }

  QString getValue() const {
    VariableType val = (*(this->m_pObject).*m_gettor)();
    QString retVal;
    ConvertFromToString::convert(val, retVal);
    return retVal;
  }

 private:

  TypeSettorFunPtrTD m_settor = nullptr;
  TypeGetterFunPtrTD m_gettor = nullptr;
  ValidatorFunPtrTD  m_validator = nullptr;

};


#endif // CLASSANDVARIABLETYPEPROPERTY_H
