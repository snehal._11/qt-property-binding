#ifndef CLASSTYPEPROPERTY_H
#define CLASSTYPEPROPERTY_H

#include "property.h"

template <class ClassType>
class ClassTypeProperty : public IProperty {
 public:

  ClassTypeProperty(const QString& name) : IProperty(name), m_pObject(nullptr) {}
  ClassTypeProperty(const QString& name, ClassType* pObject) : IProperty(name) {
    m_pObject = pObject;
  }
 public:
  ClassType* m_pObject;
};

#endif // CLASSTYPEPROPERTY_H
