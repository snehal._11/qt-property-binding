#ifndef CONTROL_H
#define CONTROL_H

#include "qboxlayout.h"
#include "qcheckbox.h"
#include "qdebug.h"
#include <QObject>
#include <QLineEdit>
#include <QComboBox>
#include "employee.h"
#include <QGroupBox>
#include <QRadioButton>





class IControl : public QObject {
  Q_OBJECT
 public:
  IControl()  {}
  virtual void setData(const QString& sData) = 0;
  virtual QString getData() const = 0;
 signals:
  void dataChanged(const QString& sData);
};

class CLineEdit : public IControl {
  Q_OBJECT
 public:
  CLineEdit(QLineEdit* lineEdit) : m_lineEdit(lineEdit) {
    connect(m_lineEdit, SIGNAL(textChanged(const QString&)), this, SIGNAL(dataChanged(const QString&)));
  }
  void setData(const QString& sData) {
    m_lineEdit->setText(sData);
  }
  QString getData() const {
    return m_lineEdit->text();
  }

 private:
  QLineEdit* m_lineEdit;

};

class CCheckBox : public IControl {
  Q_OBJECT
 public:
  CCheckBox(QCheckBox* checkBox) : m_checkBox(checkBox) {
    connect(m_checkBox, SIGNAL(stateChanged(const int&)), this, SLOT(onStateChanged(const int&)));
  }
  void setData(const QString& sData) {
    if (sData.trimmed().toUpper() == "TRUE") {
      m_checkBox->setCheckState(Qt::Checked);
    } else {
      m_checkBox->setCheckState(Qt::Unchecked);
    }
  }
  QString getData() const {

    QString checkState ;
    if (m_checkBox->checkState() == 1 ||  m_checkBox->checkState() == 2) {
      checkState = "TRUE";
    } else {
      checkState = "FALSE";
    }
    return checkState;
  }
 private slots:
  void onStateChanged(const int& val) {
    QString checkState;
    if (val == 1 || val == 2) {
      checkState = "TRUE";
    } else {
      checkState = "FALSE";
    }
    emit dataChanged(checkState);
  }
 private:
  QCheckBox* m_checkBox;
};

class CComboBox : public IControl {
  Q_OBJECT
 public:
  CEmployee m_emp;
  CComboBox(QComboBox* comboBox, const QStringList& list) : m_comboBox(comboBox) {
    m_comboBox->addItems(list);
    connect(m_comboBox, SIGNAL(currentTextChanged(const QString&)), this, SLOT(onCurrentTextChanged(const QString&)));
  }
  void setData(const QString& sData) {
    m_comboBox->setCurrentText(sData);
  }
  QString getData() const {
    return m_comboBox->currentText();
  }
 private slots:
  void onCurrentTextChanged(const QString& val) {
    emit dataChanged(val);
  }
 private:
  QComboBox* m_comboBox;
};

class CButtonGroup : public IControl {
  Q_OBJECT

 public:
  CButtonGroup(QGroupBox* buttonGroup, const QStringList& list): m_buttonGroup(buttonGroup) {
    m_buttonGroup->setCheckable(true);
    QVBoxLayout* vBox = new QVBoxLayout;
    for (int i = 0; i < list.length(); i++) {
      QRadioButton* button = new QRadioButton(list[i]);
      bool b = connect(button, SIGNAL(clicked()), this, SLOT(onClicked()));
      vBox->addWidget(button);
    }

    vBox->addStretch(1);
    m_buttonGroup ->setLayout(vBox);
  }
  void setData(const QString& sData) {
    QObjectList options = m_buttonGroup->children() ;
    foreach (QObject* option, options) {
      QRadioButton* radioButton = dynamic_cast<QRadioButton*>(option);
      if (radioButton != nullptr) {
        if (radioButton->text() == sData) {
          radioButton->setChecked(true);
          break;
        }
      }
    }

  }
  QString getData()const {
    QString retVal;
    QObjectList options = m_buttonGroup->children() ;
    foreach (QObject* option, options) {
      QRadioButton* radioButton = dynamic_cast<QRadioButton*>(option);
      if (radioButton != nullptr) {
        if (radioButton->isChecked() == true) {
          retVal = radioButton->text();
          break;
        }
      }
    }
    return retVal;
  }
 private slots:
  void onClicked() {
    QObject* obj = sender();
    QRadioButton* b2 = dynamic_cast<QRadioButton*>(obj);
    QString lan = b2->text();
    emit dataChanged(lan);
  }
 private:
  QGroupBox* m_buttonGroup;
};
#endif // CONTROL_H
