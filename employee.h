#ifndef EMPLOYEE_H
#define EMPLOYEE_H
#include "qdebug.h"
#include <QString>

class CEmployee {

 public:
  enum EMaritalSatus {
    EMaritalSatusNotDefine = -1,
    EMaritalSatusSingle,
    EMaritalSatusMarried,
    EMaritalSatusWidowed,
    EMaritalSatusDivorced
  };

  enum LanguageOption {
    LanguageOptionMarathi = 0,
    LanguageOptionHindi,
    LanguageOptionEnglish,
    LanguageOptionItalian
  };



  CEmployee() {}
  CEmployee(const CEmployee& obj) {
    bool b  = obj.getHasPassport();
  }

  void setEmployeeId(const int& EmployeeId) {
    m_EmployeeId = EmployeeId;
  }

  int getEmployeeId() const {
    return m_EmployeeId;
  }

  void setName(const QString& Name) { m_Name = Name; }
  QString getName() const { return m_Name; }

  void setLanguage(const QString& Language) { m_Language = Language; }
  QString getLanguage() const { return m_Language; }

  void setHasPassport(const bool& HasPassport) { m_hasPassport = HasPassport; }
  bool getHasPassport() const { return m_hasPassport; }

  void setMaritalStatus(const EMaritalSatus& eMaritalSatus) { m_eMaritalSatus = eMaritalSatus; }
  EMaritalSatus getMaritalStatus() const { return m_eMaritalSatus; }

  void setLanguageOption(const LanguageOption& option) {m_languageOption = option;}
  LanguageOption getLanguageOption()const {return m_languageOption;}

  QString isNameValid(const QString& name) const {
    if (name.length() < 5 || name.length() > 10)
      return "Name should be between 5 and 10 characters";
    return "";
  }

  bool isEmployeeValid() const {
    bool ret_val = false;
    if (m_EmployeeId != -1) {
      if (isNameValid(this->m_Name) == "") {
        ret_val = true;
      }
    }
    return ret_val;
  }

  static EMaritalSatus convertMaritalSatus(const QString& val) {
    EMaritalSatus eMaritalSatus = EMaritalSatus::EMaritalSatusNotDefine;
    if (val.trimmed().toUpper() == "SINGLE") {
      eMaritalSatus = EMaritalSatus::EMaritalSatusSingle;
    } else if (val.trimmed().toUpper() == "MARRIED") {
      eMaritalSatus = EMaritalSatus::EMaritalSatusMarried;
    } else if (val.trimmed().toUpper() == "WIDOWED") {
      eMaritalSatus = EMaritalSatus::EMaritalSatusWidowed;
    } else if (val.trimmed().toUpper() == "DIVORCED") {
      eMaritalSatus = EMaritalSatus::EMaritalSatusDivorced;
    }

    return eMaritalSatus;
  }

  static QString convertMaritalSatus(const EMaritalSatus& val) {
    QString maritalStatus = "Not defined";
    switch (val) {
      case EMaritalSatusSingle:
        maritalStatus = "Single";
        break;
      case    EMaritalSatusMarried:
        maritalStatus = "Married";
        break;
      case    EMaritalSatusWidowed:
        maritalStatus = "Widowed";
        break;
      case    EMaritalSatusDivorced:
        maritalStatus = "Divorced";
        break;
      default :
        maritalStatus = "Not defined";
    }
    return maritalStatus;
  }

  static LanguageOption convertLanguageOption(const QString& val) {
    LanguageOption lanOption = LanguageOption::LanguageOptionMarathi;
    if (val.trimmed().toUpper() == "MARATHI") {
      lanOption = LanguageOption::LanguageOptionMarathi;
    } else if (val.trimmed().toUpper() == "HINDI") {
      lanOption = LanguageOption::LanguageOptionHindi;
    } else if (val.trimmed().toUpper() == "ENGLISH") {
      lanOption = LanguageOption::LanguageOptionEnglish;
    } else if (val.trimmed().toUpper() == "ITALIAN") {
      lanOption = LanguageOption::LanguageOptionItalian;
    }
    return lanOption;
  }
  static QString convertLanguageOption(const LanguageOption& val) {
    QString lanOption = "Not define";
    switch (val) {
      case LanguageOptionMarathi:
        lanOption = "Marathi";
        break;
      case    LanguageOptionEnglish:
        lanOption = "English";
        break;
      case    LanguageOptionHindi:
        lanOption = "Hindi";
        break;
      case    LanguageOptionItalian:
        lanOption = "Italian";
        break;
      default :
        lanOption = "Not defined";
    }
    return lanOption;
  }
 private:
  int m_EmployeeId = -1;
  QString m_Name = 0;
  QString m_Language = "";
  EMaritalSatus m_eMaritalSatus = EMaritalSatus::EMaritalSatusNotDefine;
  bool m_hasPassport = false;
  LanguageOption m_languageOption ;
};
#endif // EMPLOYEE_H
