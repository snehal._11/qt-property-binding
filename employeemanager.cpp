#include "employeemanager.h"

#include <QDebug>

CEmployeeManager::CEmployeeManager() {
  qDebug()  << "CEmployeeManager::CEmployeeManager";
}

bool CEmployeeManager::addEmployee(const CEmployee& employee) {
  bool ret_val = employee.isEmployeeValid();
  if (ret_val) {
    foreach (auto exisingEmp, m_EmployeeList) {
      if (exisingEmp.getEmployeeId() == employee.getEmployeeId()) {
        ret_val = false;
        break;
      }
    }
  }

  if (ret_val) {
    m_EmployeeList.append(employee);
  }
  return ret_val;
}
