#ifndef CEMPLOYEEMANAGER_H
#define CEMPLOYEEMANAGER_H

#include "employee.h"
#include <QList>

class CEmployeeManager {
 public:
  static CEmployeeManager* getInstance() {
    static CEmployeeManager instance;
    return &instance;
  }
  bool addEmployee(const CEmployee& employee);
 private:
  QList<CEmployee> m_EmployeeList;
  CEmployeeManager();
};

#endif // CEMPLOYEEMANAGER_H
