#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "classandvariabletypeproperty.h"

MainWindow::MainWindow(QWidget* parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow) {

  CEmployeeManager* instance = CEmployeeManager::getInstance();
  {
    {
      CEmployee obj;
      obj.setEmployeeId(123);
      obj.setName("Joshi");
      bool b = instance->addEmployee(obj);
    }
  }
  ui->setupUi(this);
  //LineEdit
  /*  IProperty* nameProp = new ClassAndVariableTypeProperty<CEmployee, QString>("Name", &CEmployee::setName,
                                                                               &CEmployee::getName,
                                                                               &m_emp, &CEmployee::isNameValid);
    CBinder* NameBinder = new CBinder(
        nameProp,
        new CLineEdit(ui->m_txtSource), ui->m_errorButton);
    BinderList.append(NameBinder);*/
  BinderList.append(new CBinder(
                        (new ClassAndVariableTypeProperty<CEmployee, QString>("Name", &CEmployee::setName,
                                                                              &CEmployee::getName,
                                                                              &m_emp, &CEmployee::isNameValid)),
                        new CLineEdit(ui->m_txtSource), ui->m_errorButton));

//CheckBox
  BinderList.append(new CBinder((new ClassAndVariableTypeProperty<CEmployee, bool>("Passport", &CEmployee::setHasPassport,
                                 &CEmployee::getHasPassport,
                                 &m_emp)),
                                new CCheckBox(ui->m_checkBox)));
  //ComboBox
  QStringList list = {m_emp.convertMaritalSatus(CEmployee::EMaritalSatus::EMaritalSatusNotDefine),
                      m_emp.convertMaritalSatus(CEmployee::EMaritalSatus::EMaritalSatusSingle),
                      m_emp.convertMaritalSatus(CEmployee::EMaritalSatus::EMaritalSatusMarried),
                      m_emp.convertMaritalSatus(CEmployee::EMaritalSatus::EMaritalSatusDivorced),
                      m_emp.convertMaritalSatus(CEmployee::EMaritalSatus::EMaritalSatusWidowed)
                     } ;
  BinderList.append(new CBinder(new ClassAndVariableTypeProperty<MainWindow, QString>
                                ("Marital_Status", &MainWindow::setMaritalStatus,
                                 &MainWindow::getMaritalStatus,
                                 this, &MainWindow::isMaritalStatusValid),
                                new CComboBox(ui->m_comboBox, list), ui->cErrorButton));
//ButtonGroup(RadioButton)
  QStringList optionList = {m_emp.convertLanguageOption(CEmployee::LanguageOption::LanguageOptionMarathi),
                            m_emp.convertLanguageOption(CEmployee::LanguageOption::LanguageOptionEnglish),
                            m_emp.convertLanguageOption(CEmployee::LanguageOption::LanguageOptionHindi),
                            m_emp.convertLanguageOption(CEmployee::LanguageOption::LanguageOptionItalian)
                           };
  BinderList.append(new CBinder(new ClassAndVariableTypeProperty<MainWindow, QString>("language", &MainWindow::setLanguage,
                                &MainWindow::getLanguage,
                                this),
                                new CButtonGroup(ui->m_groupBox, optionList)));

  foreach (CBinder* binderObj, BinderList) {
    connect(binderObj, SIGNAL(statusChanged()), this, SLOT(onBinderStatusChanged()));
  }
  //init() :
  foreach (CBinder* binderObj, BinderList) {
    binderObj->init();
  }
}

MainWindow::~MainWindow() {
  delete ui;
}

void MainWindow::setMaritalStatus(const QString& val) {
  m_emp.setMaritalStatus(CEmployee::convertMaritalSatus(val));
}

QString MainWindow::getMaritalStatus() const {
  return CEmployee::convertMaritalSatus(m_emp.getMaritalStatus());
}

void MainWindow::setLanguage(const QString& val) {
  m_emp.setLanguageOption(CEmployee::convertLanguageOption(val));
}

QString MainWindow::getLanguage() const {
  return CEmployee::convertLanguageOption(m_emp.getLanguageOption());
}

void MainWindow::onBinderStatusChanged() {
  bool isWindowValid = true;
  foreach (CBinder* binderObj, BinderList) {
    if (binderObj->getPropertyStatus() == IProperty::Status::Invalid) {
      isWindowValid = false;
      break;
    }
  }
  ui->pushButton->setDisabled(!isWindowValid);
}
void MainWindow::on_pushButton_clicked() {
  QMessageBox mBox;
  QString empData;
  bool hasPassport = m_emp.getHasPassport();

  empData.append("Name : " + m_emp.getName());
  empData.append("\nMaritalStatus : " + getMaritalStatus());
  if (hasPassport)
    empData.append("\nHasPassport : True");
  else
    empData.append("\nHasPassport : False");
  empData.append("\nLanguage : " + getLanguage());
  mBox.setWindowTitle("Data");
  mBox.setText(empData);
  mBox.exec();
}
