#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "binder.h"
#include "employee.h"
#include "employeemanager.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  enum EWindowStatus {
    eStatusNotDefined = -1,
    eStatusValid,
    eStatusInvalid
  };

  MainWindow(QWidget* parent = nullptr);
  ~MainWindow();


 private:
  Ui::MainWindow* ui;
  QList<CBinder*> BinderList ;
  CEmployee m_emp;
  EWindowStatus m_eWindowStatus = eStatusNotDefined;

 public:
  void setMaritalStatus(const QString& val);
  QString getMaritalStatus()const;

  void setLanguage(const QString& val);
  QString getLanguage()const;


  QString isMaritalStatusValid(const QString& maritalStatus) const {
    QString error = "";
    CEmployee::EMaritalSatus eMaritalStatus = m_emp.convertMaritalSatus(maritalStatus);
    if (eMaritalStatus == CEmployee::EMaritalSatus::EMaritalSatusNotDefine) {
      error = "Select a value.";
    }
    return error;
  }
 private slots:
  void on_pushButton_clicked();
  void onBinderStatusChanged();

};

#endif // MAINWINDOW_H
