#ifndef PROPERTY_H
#define PROPERTY_H
#include <QObject>
#include <QString>
#include "employee.h"
using namespace std;

class ConvertFromToString {
 public:
  static void convert(const QString& val, int& converted_value) {
    converted_value = val.toInt();
  }
  static void convert(const int& val, QString& converted_value) {
    converted_value = QString::number(val);
  }
  static void convert(const QString& val, QString& converted_value) {
    converted_value = val;
  }
  static void convert(const bool& val, QString& converted_value) {
    if (val) {
      converted_value = "TRUE";
    } else {
      converted_value = "FALSE";
    }
  }
  static void convert(const QString& val, bool& converted_value) {
    if (val.toUpper() == "TRUE") {
      converted_value = true;
    } else {
      converted_value = false;
    }
  }
};


class IProperty : public QObject {
  Q_OBJECT
 public:
  enum Status {
    NoChange = 0,
    Invalid,
    Valid
  };

  IProperty(const QString& name) {
    m_name = name;
  }

  virtual IProperty* operator = (const QString& val) = 0;
  virtual QString isPropertyValid() const = 0;
  virtual QString getValue() const = 0;
  Status getStatus()const {
    return m_status;
  }

 signals:
  void propertyChanged();
  void statusChanged();

 protected:
  void setStatus(const IProperty::Status& st) {
    if (st != m_status) {
      m_status = st;
      emit statusChanged();
    }
  }


 private:
  Status m_status = Status::NoChange;
  QString m_name;

};

#endif // PROPERTY_H
